# icms-common-vue

The repository contains a collection of components and utilities shared between the different SPA's of icms.

## Installation
The intended way to include the common package to a project is as a git submodule.

To add `icms-common-vue` in a project run : 
```console
$ git submodule add ssh://git@gitlab.cern.ch:7999/cms-icmsweb/icms-common-vue.git
```

### Creating a webpack alias
For projects created with `vue-cli` it might be useful to create a webpack alias for the `icms-common-vue` folder.
Add the following in `vue.config.js`:
```
var path = require("path");

module.exports = {
  ...
  configureWebpack: {
    resolve: {
      alias: {
        common: path.resolve(__dirname, "icms-common-vue"),
      },
    },
  },
};
```
Then importing from `icms-common-vue` can be done without using relative paths
```
import {...} from 'icms-common-vue'
```   

## Git submodules
Submodules are Git repositories nested inside a parent Git repository at a specific path in the parent repository's working directory.

The following is a brief guide of how to perform common tasks on a project using submodules . A more detailed guide into git submodules and details on the inner-workings can be found in [the official documentation](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

### Adding a submodule to a repository 
To add a new submodule you use the `git submodule add` command with the absolute or relative URL of the project you would like to start tracking and optionally a path.
```
$ git submodule add <url> <path>
```

### Cloning a Project with Submodules
When you clone such a project, by default you get the directories that contain submodules, but none of the files within them yet.
After cloning the project run :
```
$ git submodule update --init
```
Alternatively If you pass `--recurse-submodules` to the `git clone` command, it will automatically initialize and update each submodule in the repository.
 ```
$ git clone --recurse-submodules <url>
```

### Pulling in Upstream Changes from the Submodule Remote
If you want to check for new work in a submodule, you can go into the directory and run  `git fetch`  and  `git merge`  the upstream branch to update the local code.
There is an easier way to do this as well, if you prefer to not manually fetch and merge in the subdirectory. If you run:
 ```
 $ git submodule update --remote
 ``` 
 Git will go into your submodules and fetch and update for you.

### Pulling Upstream Changes from the Project Remote
By default, the `git pull` command recursively fetches submodules changes, as we can see in the output of the first command above. However, it does not **update** the submodules.
To finalize the update, you need to run:
```
$ git submodule update --init --recursive
```

### Working on a Submodule
Inside the submodule's directory, you can create a new branch with `git checkout -b <branch_name>` and make any changes or additions. After your branch is merged remember to switch back to master and run `git submodule update --remote`.

## Contents
A brief overview and some notes on the contents of `icms-common-vue`

### Components
The package contains some commonly used components including : 
* AppBar
* AppFooter
* AppMenu
* PortalDropdown
* AppNotification
* BasePage
* DataTable
* ScreenSizeWarning

Those can be either imported one by one:
```
import { AppMenu } from 'common/components/AppMenu'
``` 
Or installed globally using the the plugin that is the default export of the common folder:
```
// main.js

import  plugin  from  "common";
...
Vue.use(plugin, options);

``` 

#### PortalDropdown
A component used for navigating between all the iCMS apps.
`current` prop specifies which app is displayed as the current option and is required.
For an app to appear in the dropdown menu as an option a specific environment variable containing the apps url must be present in the .env file.

The available options are: 
* for `icms-tools` :  `VUE_APP_TOOLS_URL`
* for `icms-statistics` :  `VUE_APP_STATS_URL`
* for `icms-monitoring` :  `VUE_APP_MONITORING_URL`

### Utilities
#### Example for creating an ApiCall class 
```
import { createAxiosInstance, BaseCall } from  "common";

export  const  axiosHandler = createAxiosInstance({
  baseUrl: <my-api-url>,
  config: < Additional configutation of the axios instance, optional >,
  defaultErrorHandler: < An optional callback for handling error responses ( Ex: (err) => window.alert('An Error has occurred')) >
});

class ApiCall extends BaseCall {
	constructor(endpoint){
		super(axiosHandler, endpoint)
	}
}
``` 
### Example using AppNotifications with vuex
The `AppNotification` component can be used either standalone by passing the  `type` and `message` props 
```
<app-notification :type="type" :message="message" />
```
Or in conjuction with the `notificationModule` vuex module by setting the `subscribeToStore` prop to `true`

```
// store.js
import { notificationModule } from  "common";

Vue.use(Vuex);
export  default  new  Vuex.Store({
	modules: {
		notification:  notificationModule,
		... other modules
	}
})
```
```
// App.vue
...
<app-notification :subscribeToStore="true" />
```
