export class BaseCall {
  constructor(
    axiosInstance,
    endpoint,
    successCallback = null,
    failureCallback = null
  ) {
    this.axiosInstance = axiosInstance;

    this.endpoint = endpoint;

    this.succesCallback = successCallback;
    this.failureCallback = failureCallback;
    this.data = {};
    this.processData = true;
    this.contentType = false;
    this.dataType = "json";
  }

  static isValidUrl(endpoint) {
    return endpoint.startsWith("http://") || endpoint.startsWith("https://");
  }

  getEndpoint() {
    return this.endpoint;
  }

  setId(id) {
    /**
     * For scenarios where resource is accessed like /xyz/resource/id,
     * appends id at the end of the path.
     */
    if (id !== null) {
      this.endpoint = this.endpoint + "/" + id;
    }
    return this;
  }

  set(paramName, paramValue) {
    this.data[paramName] = paramValue;
    return this;
  }

  setData(data) {
    this.data = data;
    return this;
  }

  setSuccessCallback(callable) {
    this.succesCallback = callable;
    return this;
  }

  setFailureCallback(callable) {
    this.failureCallback = callable;
    return this;
  }

  get() {
    return this.ajaxCall("get", {});
  }

  post() {
    return this.ajaxCall("post");
  }

  put() {
    return this.ajaxCall("put");
  }

  delete() {
    return this.ajaxCall("delete", {});
  }

  initAjaxCall(type) {
    return this.axiosInstance[type](this.endpoint, this.data);
  }

  ajaxCall(type, headers = null, data = null) {
    const promise = this.initAjaxCall(type, headers, data);
    if (this.succesCallback) {
      promise.then((data) => this.succesCallback(data));
    }
    if (this.failureCallback) {
      promise.catch((data) => this.failureCallback(data));
    }
    return promise;
  }

  removeBlankFields() {
    /**
     * Can be used to prevent null values from being submitted with a request.
     */
    Object.keys(this.data).forEach(
      k => (this.data[k] == null || this.data[k] == undefined) && delete this.data[k]
    );
    return this;
  }

}
export default BaseCall;
