/**
 * The why: some resources contain references to resources living across an AR / microservice boundary
 * (eg. CMS IDs describing the note submitters, editors etc)
 *
 * As we dynamically load the former, we accumulate the IDs of the latter.
 * Retrieving the associated resources one by one or all at once is generally far from ideal.
 *
 * So we need a store of sorts, loading the additional resources and describing them to the clients.
 *
 * The current best practice is to use a resolver object, like the CmsIdResolver:
 * - instantiate it within a component
 * - feed it with IDs needing resolution
 * - watch for changes in its size and react to them by calling fetchMissing()
 * - get people labels from the resolver
 *
 * This will hopefully evolve to further reduce the boilerplate and integrate better with the reactivity
 * system (through Vuex perhaps?). As of now though it's good enough to carry on.
 *
 *
 * Meanwhile the base IdResolver can still be used from inside a component, at the expense of handling all
 * the necessary data retrieval and putting it into the resolver.
 */
class IdResolver {
  constructor(parentComponent, idPropName = "id") {
    this.compo = parentComponent;
    this.data = {};
    this.idPropName = idPropName;
  }
  getLabelForId(value) {
    return value;
  }
  getDataForId(value) {
    return this.data[value];
  }
  size() {
    return Object.keys(this.data).length;
  }
  markIdForResolution(value) {
    if (this.data[value] === undefined) {
      this.data[value] = null;
    }
  }
  storeRetrieved(items) {
    var idPropName = this.idPropName;
    var incoming = Object.fromEntries(items.map((e) => [e[idPropName], e]));
    // store resolved IDs, incoming should overwrite the existing
    this.data = { ...this.data, ...incoming };
  }
  markIdsForResolution(arr) {
    // accumulates the ids that need resolution, retains existing values
    this.data = {
      ...Object.fromEntries(arr.map((e) => [e, null])),
      ...this.data,
    };
  }
  getIdsToResolve() {
    return Object.entries(this.data)
      .filter((e) => e[1] === null)
      .flatMap((e) => e[0]);
  }
  fetchMissing() {
    console.debug(
      "I'm a base class and I don't know how to do that. Subclass me."
    );
  }
}
export default IdResolver;
