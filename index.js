export { default as notificationModule } from "./store/notification";

export { default as createAxiosInstance } from "./api/createAxiosInstance";
export { default as BaseCall } from "./api/BaseCall";
export { default as Hateoas } from "./api/Hateoas";
export { default as IdResolver } from "./api/IdResolver";

import Vuetify from "vuetify/lib";
import {
  AppBar,
  BasePage,
  DataTable,
  AppFooter,
  AppMenu,
  AppNotification,
  ScreenSizeWarning,
  PortalDropdown,
} from "./components";

const vuetify = new Vuetify({});

export default {
  install(Vue, options) {
    Vue.component("base-page", BasePage);
    Vue.component("data-table", DataTable);
    Vue.component("app-footer", AppFooter);
    Vue.component("app-bar", AppBar);
    Vue.component("app-menu", AppMenu);
    Vue.component("app-notification", AppNotification);
    Vue.component("screen-size-warning", ScreenSizeWarning);
    Vue.component("portal-dropdown", PortalDropdown);

    Vue.use(Vuetify, {
      options: {
        customProperties: true,
      },
    });
    options.vuetify = vuetify;
  },
};
