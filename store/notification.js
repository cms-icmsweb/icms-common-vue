const state = () => ({
  type: null,
  message: null,
});

const mutations = {
  showMessage(state, { type, message }) {
    state.type = type;
    state.message = message;
  },
};

const actions = {
  infoMessage({ commit }, message) {
    commit("showMessage", { type: "info", message });
  },
  successMessage({ commit }, message) {
    commit("showMessage", { type: "success", message });
  },
  warnMessage({ commit }, message) {
    commit("showMessage", { type: "warn", message });
  },
  errorMessage({ commit }, message) {
    commit("showMessage", { type: "error", message });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
